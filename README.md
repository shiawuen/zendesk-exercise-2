
## Task management app

You can view the [hosted solution](http://zendesk-exercise-2.s3-website-ap-southeast-1.amazonaws.com/) which I put up on AWS S3.

Exercise instructions: https://gist.github.com/aravindet/ce19c019b774794c4e23df5760e8cca5

### Prerequisites

You need to have [Node.js](https://nodejs.org/en/) installed on your computer.
You need to have your Terminal ready to execute some command line instructions.

### Settings up

Clone this repo to your local with command below

```
git clone git@gitlab.com:shiawuen/zendesk-exercise-2.git
cd zendesk-exercise-2
npm install
```

Wait for awhile for the dependencies download

### Running the app

To run the app, simply execute command below in your terminal

```
npm run start
```
