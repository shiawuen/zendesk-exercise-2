import { combineReducers } from 'redux'
import boards from './boards/index'
import tasks from './tasks/index'

export default combineReducers({ boards, tasks })

export const getAllBoards = (state) =>
  state.boards.allIds

export const getBoardById = (state, id) =>
  state.boards.byId[id]

export const getAllTasks = (state) =>
  state.tasks.allIds

export const getTaskById = (state, id) =>
  state.tasks.byId[id]

export const getBoardWithTaskId = (state, taskId) => {
  const boardId = state.boards.allIds.find(id =>
    state.boards.byId[id].tasks.includes(taskId)
  )
  return state.boards.byId[boardId]
}

export const getTaskIndexInBoard = (state, taskId) =>
  getBoardWithTaskId(state, taskId).tasks.indexOf(taskId)

export const willInsertAtSameLocation = (
  state,
  fromTaskId,
  toTaskId,
  targetBoardId,
  moveBy,
  insertAfter
) => {
  const board = getBoardWithTaskId(state, fromTaskId)
  if (board.id !== targetBoardId) return false

  const currentIndex = getTaskIndexInBoard(state, fromTaskId)
  if (!toTaskId) return currentIndex === 0
  const targetIndex = moveBy + getTaskIndexInBoard(state, toTaskId)
  return currentIndex === targetIndex
}
