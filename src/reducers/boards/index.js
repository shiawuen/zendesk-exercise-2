import { combineReducers } from 'redux'
import byId from './by-id'
import allIds from './all-ids'

export default combineReducers({ byId, allIds })
