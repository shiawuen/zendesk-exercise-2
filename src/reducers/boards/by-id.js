import board from './board'

export default (state = {}, action) => {
  switch (action.type) {
    case 'ADD_BOARD':
      return {
        ...state,
        [action.board.id]: board(state[action.board.id], action)
      }
    case 'ADD_BOARD_TASK':
    case 'REMOVE_BOARD_TASK':
      return {
        ...state,
        [action.board.id]: board(state[action.board.id], action)
      }
    default:
      return state
  }
}