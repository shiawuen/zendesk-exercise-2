
export default (state = [], action) => {
  switch (action.type) {
    case 'ADD_BOARD':
      return [ ...state, action.board.id ]
    default:
      return state
  }
}
