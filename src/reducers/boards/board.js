
export default (state, action) => {
  switch (action.type) {
    case 'ADD_BOARD':
      return action.board
    case 'REMOVE_BOARD_TASK':
      return {
        ...state,
        tasks: state.tasks.filter(id => action.task.id !== id)
      }
    case 'ADD_BOARD_TASK':
      const { targetTask = {}, moveBy } = action
      let tasks = state.tasks
      if (!targetTask.id) {
        tasks = [ action.task.id, ...tasks ]
      } else {
        const targetIndex = moveBy + tasks.indexOf(targetTask.id)
        tasks = [
          ...tasks.slice(0, targetIndex),
          action.task.id,
          ...tasks.slice(targetIndex, tasks.length)
        ]
      }
      return { ...state, tasks }
    default:
      return state
  }
}
