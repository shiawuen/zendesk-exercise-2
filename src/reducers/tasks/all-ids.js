
export default (state = [], action) => {
  switch (action.type) {
    case 'ADD_TASK':
      return [ ...state, action.task.id ]
    default:
      return state
  }
}
