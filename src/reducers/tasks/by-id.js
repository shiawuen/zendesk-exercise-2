import task from './task'

export default (state = {}, action) => {
  switch (action.type) {
    case 'ADD_TASK':
      return {
        ...state,
        [action.task.id]: task(state[action.task.id], action)
      }
    default:
      return state
  }
}
