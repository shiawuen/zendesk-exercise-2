
export default (state, action) => {
  const { type, task } = action
  switch (type) {
    case 'ADD_TASK':
      return task
    default:
      return state
  }
}
