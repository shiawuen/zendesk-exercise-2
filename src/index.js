import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import './index.css'
import { Provider } from 'react-redux'
import configureStore from './configureStore'
import { addBoard } from './actions'

const store = configureStore()

// Add default boards of list
addBoard('To do')(store.dispatch)
addBoard('In progress')(store.dispatch)
addBoard('Done')(store.dispatch)

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)
