import cuid from 'cuid'
import * as selectors from '../reducers'
import throttle from 'lodash/throttle'

export const addBoard = (title) => (dispatch) => dispatch({
  board: { title, id: cuid(), tasks: [] },
  type: 'ADD_BOARD'
})

export const addTask = (boardId, title) => (dispatch) => {
  const id = cuid()
  dispatch({
    task: { title, id },
    type: 'ADD_TASK',
  })
  dispatch({
    task: { id },
    board: { id: boardId },
    type: 'ADD_BOARD_TASK',
  })
}

export const moveTask = (() => {
  let cancelThrottle
  const wait = 300
  return ({ taskId, targetTaskId, targetBoardId, moveBy = 0 }) => {
    return (dispatch, getState) => {
      if (cancelThrottle) cancelThrottle()
      cancelThrottle = throttle(() => {
        if (taskId === targetTaskId) return

        const state = getState()
        if (selectors.willInsertAtSameLocation(
          state,
          taskId,
          targetTaskId,
          targetBoardId,
          moveBy
        )) return

        const fromBoard = selectors.getBoardWithTaskId(state, taskId)
        dispatch({
          board: { id: fromBoard.id },
          task: { id: taskId },
          type: 'REMOVE_BOARD_TASK',
        })
        dispatch({
          moveBy,
          board: { id: targetBoardId },
          task: { id: taskId },
          targetTask: { id: targetTaskId },
          type: 'ADD_BOARD_TASK',
        })
      }, wait)
    }
  }
})()
