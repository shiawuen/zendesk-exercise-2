import React from 'react'
import { connect } from 'react-redux'
import TaskCount from './TaskCount'
import { getAllTasks } from '../reducers'

let TotalTasks = ({ count }) => (
  <TaskCount label='Total' count={count} />
)

const mapStateToProps = (state) => ({ count: getAllTasks(state).length })

export default connect(mapStateToProps)(TotalTasks)
