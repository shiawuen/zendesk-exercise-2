import React from 'react'
import AddTask from './AddTask'
import TotalTasks from './TotalTasks'
import './MainHeader.css'

let MainHeader = () => (
  <header className='MainHeader'>
    <AddTask />
    <TotalTasks />
  </header>
)

export default MainHeader
