import React from 'react'
import HTML5Backend from 'react-dnd-html5-backend'
import TouchBackend from 'react-dnd-touch-backend'
import { DragDropContext } from 'react-dnd'
import Boards from './Boards'
import MainHeader from './MainHeader'
import './App.css'

// ref: http://stackoverflow.com/questions/4817029/whats-the-best-way-to-detect-a-touch-screen-device-using-javascript
const isTouch = 'ontouchstart' in window || navigator.maxTouchPoints

const dragDropContext = DragDropContext(
  isTouch ? TouchBackend : HTML5Backend
)

export default dragDropContext(() => (
  <div className='App'>
    <MainHeader />
    <Boards />
  </div>
))