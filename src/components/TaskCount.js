import React from 'react'
import './TaskCount.css'

let TaskCount = ({ count, label = '', unit = 'project' }) => (
  <span className='TaskCount'>
    {label} {count} {unit}{count === 1 ? '' : 's'}
  </span>
)

export default TaskCount