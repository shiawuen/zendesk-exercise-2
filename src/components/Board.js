import React from 'react'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { DropTarget } from 'react-dnd'
import Task from './Task'
import { getBoardById } from '../reducers'
import { moveTask } from '../actions'
import TaskCount from './TaskCount'
import './Board.css'

const Board = ({
  connectDropTarget,
  moveTask,
  board
}) => connectDropTarget(
  <section className='Board'>
    <header className='Board__header'>
      <h3>{board.title}</h3>
      <TaskCount count={board.tasks.length} />
    </header>
    <div>
      {board.tasks.map((id, order) =>
        <Task {...{ id, board }}
          key={id}
          moveTask={(taskId, moveBy) => {
            moveTask({
              taskId, moveBy,
              targetBoardId: board.id,
              targetTaskId: id,
            })
          }}
        />
      )}
    </div>
  </section>
)

const mapStateToProps = (state, { id }) => ({
  board: getBoardById(state, id)
})

const hoverHandler = ({ board, moveTask }, monitor) => {
  // Ignore if it's not a direct hover
  if (!monitor.isOver({ shallow: true })) return

  const dragItem = monitor.getItem()
  // Ignore if task already in this board
  if (board.tasks.includes(dragItem.id)) return

  moveTask({
    taskId: monitor.getItem().id,
    targetBoardId: board.id
  })
}
const dropTarget = DropTarget('TASK',
  { hover: hoverHandler },
  (connect) => ({ connectDropTarget: connect.dropTarget() })
)

export default compose(
  connect(mapStateToProps, { moveTask }),
  dropTarget
)(Board)
