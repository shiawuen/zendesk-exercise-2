import React, { Component } from 'react'
import { findDOMNode } from 'react-dom'
import { compose } from 'redux'
import { connect } from 'react-redux'
import { DragSource, DropTarget } from 'react-dnd'
import { getTaskById } from '../reducers'
import './Task.css'

class Task extends Component {
  render () {
    return compose(
      this.props.connectDragSource,
      this.props.connectDropTarget
    )(<div className='Task'>{this.props.task.title}</div>)
  }
}

const dragSource = DragSource('TASK',
  { beginDrag({ task }) { return task } },
  (connect) => ({ connectDragSource: connect.dragSource() })
)

const hoverHandler = ({ task, moveTask }, monitor, component) => {
  const dragTask = monitor.getItem()
  if (dragTask.id === task.id) return

  const boundingRect = findDOMNode(component).getBoundingClientRect()
  const clientOffset = monitor.getClientOffset()
  const midPoint = (boundingRect.bottom - boundingRect.top) / 2
  const fromTop = monitor.getInitialClientOffset().y < clientOffset.y
  const cursorLocation = clientOffset.y - boundingRect.top
  const overMid = cursorLocation > midPoint
  const moveBy = overMid
    ? fromTop ? 1 : 0
    : fromTop ? -1 : 0
  moveTask(dragTask.id, moveBy)
}

const dropTarget = DropTarget('TASK',
  { hover: hoverHandler },
  (connect, monitor) => ({ connectDropTarget: connect.dropTarget() })
)

export default compose(
  connect((state, { id }) => ({ task: getTaskById(state, id) })),
  dragSource,
  dropTarget
)(Task)
