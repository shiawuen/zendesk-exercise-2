import React from 'react'
import { connect } from 'react-redux'
import { addTask } from '../actions'
import { getAllBoards } from '../reducers'
import Input from './Input'
import './AddTask.css'

let AddTask = ({ addTask, boardId }) => (
  <div className='AddTask'>
    <Input 
      label='Click to add project'
      onSubmit={value => addTask(boardId, value)}
    />
  </div>
)

const mapStateToProps = (state) => ({
  boardId: getAllBoards(state)[0]
})

export default connect(mapStateToProps, { addTask })(AddTask)
