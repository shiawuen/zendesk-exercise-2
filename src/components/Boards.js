import React from 'react'
import { connect } from 'react-redux'
import Board from './Board'
import { getAllBoards } from '../reducers'
import './Boards.css'

const mapStateToProps = (state) => ({
  boards: getAllBoards(state)
})

const Boards = ({ boards }) => (
  <div className='Boards'>
    {boards.map(id => <Board key={id} id={id} />)}
  </div>
)

export default connect(mapStateToProps)(Boards)
