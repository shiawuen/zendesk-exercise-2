import React from 'react'
import './Input.css'

const Input = ({ label, value, onSubmit }) => (
  <input
    className='Input'
    placeholder={label}
    defaultValue={value}
    onKeyUp={event => {
      if (event.keyCode !== 13 || event.target.value === '') return
      onSubmit(event.target.value)
      event.target.value = ''
    }}
  />
)

export default Input
